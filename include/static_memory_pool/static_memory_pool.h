#ifndef _STATIC_MEMORY_POOL_H_
#define _STATIC_MEMORY_POOL_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "static_stack/static_stack.h"

/**
 * @brief header of the static memory pool used internaly
 * 
 */
typedef struct {
    sstack_t* stack;    /**< pointer to the internal stack */
    size_t elem_size;   /**< size of one element in byte */ 
    size_t capacity;    /**< max number of elements can be stored */
} smpool_header_t;
 
/**
 * @brief helper macro for reserving enought memory
 * 
 * @param capacity specifie the number of elements the stack can hold
 * @param elem_size specifie the size of one element in byte
 */
#define smpool_reserve(capacity, elem_size) (sstack_reserve(capacity, sizeof(void*)) + sizeof(smpool_header_t) + ((size_t)(capacity)) * (size_t)(elem_size))

/**
 * @brief type alias for static memory pool instance
 * 
 */
typedef uint8_t smpool_t;

/**
 * @brief init the static memory pool
 * 
 * @param smp static memory pool instance 
 * @param capacity specifie the number of elements the stack can hold 
 * @param elem_size specifie the size of one element in byte 
 * @return true on success
 * 
 * @sa smpool_reserve
 * @note capacity && elem_size musst be the same as in smpool_reserve macro
 */
bool smpool_init(smpool_t* smp, size_t capacity, size_t elem_size);

/**
 * @brief aquire a memory chunk from the static memory pool
 * 
 * @param smp static memory pool instance 
 * @param e pointer of the pointer where the empty memory chunk is
 * @return true on success  
 */
bool smpool_aquire(smpool_t* smp, void** e);

/**
 * @brief release a memory chunk which where aquired before
 * 
 * @param smp static memory pool instance 
 * @param e pointer of the aquired memory from the memory pool 
 * @return true on success  
 */
bool smpool_release(smpool_t* smp, void* e);

/**
 * @brief get the number of memory chunks left in the memory pool
 * 
 * @param smp static memory pool instance 
 * @param size pointer to memory where the size will be stored
 * @return true on success  
 */
bool smpool_size(smpool_t* smp, size_t* size);

/**
 * @brief get the number of memory chunks the static memory pool can prived
 * 
 * @param smp static memory pool instance 
 * @param capacity pointer to memory where the capacity will be stored 
 * @return true on success  
 */
bool smpool_capacity(smpool_t* smp, size_t* capacity);

#ifdef __cplusplus
}
#endif

#endif // _STATIC_MEMORY_POOL_H_