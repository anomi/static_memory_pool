#include "static_memory_pool/static_memory_pool.h"

bool smpool_init(smpool_t* smp, size_t capacity, size_t elem_size) {
    bool b = false;
    smpool_header_t* h = (smpool_header_t*)smp;
    if (smp != NULL
        && capacity > 0
        && elem_size > 0)
    {
        h->stack = smp + sizeof(smpool_header_t);
        uint8_t* data = smp + sizeof(smpool_header_t) + sstack_reserve(capacity, sizeof(void*));
        h->elem_size = elem_size;
        h->capacity = capacity;
        // init the stack structure
        if (sstack_init(h->stack, capacity, sizeof(void*)) == true)
        {
            b = true;
            for (size_t i = 0; i < capacity; i++)
            {
                size_t e = (size_t)(data + i * elem_size);
                // fill the stack with start addresses of the free memory chunks
                if (sstack_push(h->stack, &e) != true)
                {
                    b = false;
                    break;
                }
            }
        }
    }
    return b;
}

bool smpool_aquire(smpool_t* smp, void** e) {
    bool b = false;
    smpool_header_t* h = (smpool_header_t*)smp;
    if (smp != NULL
        && e != NULL)
    {
        size_t elem = 0;
        // try to get a address of a free chunk
        if (sstack_pop(h->stack, &elem) == true)
        {
            *e = (void*)elem;
            b = true;
        }
    }
    return b;
}

bool smpool_release(smpool_t* smp, void* e) {
    bool b = false;
    smpool_header_t* h = (smpool_header_t*)smp;
    size_t addr = (size_t)e;
    if (smp != NULL
        && e != NULL)
    {
        uint8_t* data = smp + sizeof(smpool_header_t) + sstack_reserve(h->capacity, sizeof(void*));
        
        // check if the given e is in range of the memory chunk address
        if (addr >= (size_t)(data + 0 * h->elem_size)
            && addr <= (size_t)(data + (h->capacity - 1) * h->elem_size))
        {
            // put the chunk address back into the stack
            if (sstack_push(h->stack, &addr) == true)
            {
                b = true;
            }
        }
    }
    return b;
}

bool smpool_size(smpool_t* smp, size_t* size) {
    bool b = false;
    smpool_header_t* h = (smpool_header_t*)smp;
    if (smp != NULL
        && size != NULL)
    {
        size_t capacity;
        if (smpool_capacity(smp, &capacity) == true
            && sstack_size(h->stack, size) == true)
        {
            *size = capacity - *size; 
            b = true;
        }
    }
    return b;
}

bool smpool_capacity(smpool_t* smp, size_t* capacity) {
    bool b = false;
    smpool_header_t* h = (smpool_header_t*)smp;
    if (smp != NULL
        && capacity != NULL)
    {
        b = sstack_capacity(h->stack, capacity);
    }
    return b;
}