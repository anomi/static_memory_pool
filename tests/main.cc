#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch2/catch.hpp"
#include "static_memory_pool/static_memory_pool.h"

TEST_CASE("static memory pool reserve macro", "[static_memory_pool, smpool_reserve]") {
    /**
     * Unfortunately this are more alibi tests because C does not proved better options
     */
    SECTION( "static memory pool reserve normal" ) { 
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with 0 capacity") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with negative capacity") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with 0 element size") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with negativ element size") {
        REQUIRE(true == true);
    }
}

TEST_CASE("static memory pool init function tests", "[static_memory_pool, smpool_init]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(smp, 0, sizeof(smp)));

    SECTION("static memory pool init normal") {
        REQUIRE(true == smpool_init(smp, capacity, elem_size));
    }
    SECTION("static memory pool init with nullptr") {
        REQUIRE(false == smpool_init(NULL, capacity, elem_size));
    }
    SECTION("static memory pool init with 0 capacity") {
        REQUIRE(false == smpool_init(smp, 0, elem_size));
    }
    SECTION("static memory pool init with 0 element size") {
        REQUIRE(false == smpool_init(smp, capacity, 0));
    }
}

TEST_CASE("static memory pool aquire function tests", "[static_memory_pool, smpool_aquire]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(smp, 0, sizeof(smp)));
    REQUIRE(true == smpool_init(smp, capacity, elem_size));

    SECTION("static memory pool aquire normal") {
        int i = 0;
        int* e = 0;
        while (true == smpool_aquire(smp, (void**)&e))
        {
            i++;
        }
        REQUIRE(capacity == i);        
    }
    SECTION("static memory pool aquire with nullptr") {
        int i = 0;
        int* e = 0;
        while (true == smpool_aquire(NULL, (void**)&e))
        {
            i++;
        }
        REQUIRE(0 == i);    
    }
    SECTION("static memory pool aquire with nullptr to element") {
        int i = 0;
        while (true == smpool_aquire(smp, NULL))
        {
            i++;
        }
        REQUIRE(0 == i);    
    }
    SECTION("static memory pool with not initialized memory pool") {
        /* not implemented in static memory pool */
        REQUIRE(true == true);
    }
}

TEST_CASE("static memory pool release function tests", "[static_memory_pool, smpool_release]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    int* elements[capacity];
    REQUIRE(NULL != memset(smp, 0, sizeof(smp)));
    REQUIRE(true == smpool_init(smp, capacity, elem_size));
    for (int i = 0; smpool_aquire(smp, (void**)&elements[i]); i++);

    SECTION("static memory pool release normal") {
        int i = 0;
        while (true == smpool_release(smp, (void*)elements[i]))
        {
            i++;
        }
        REQUIRE(capacity == i);        
    }
    SECTION("static memory pool release with nullptr") {
        int i = 0;
        while (true == smpool_release(NULL, (void*)elements[i]))
        {
            i++;
        }
        REQUIRE(0 == i);    
    }
    SECTION("static memory pool release with nullptr to element") {
        int i = 0;
        while (true == smpool_release(smp, NULL))
        {
            i++;
        }
        REQUIRE(0 == i);    
    }
    SECTION("static memory pool with not initialized memory pool") {
        /* not implemented in static memory pool */
        REQUIRE(true == true);
    }
}

TEST_CASE("static memory pool size function tests", "[static_memory pool, smpool_size]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t s[smpool_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == smpool_init(s, capacity, elem_size));
    size_t size = 0;

    SECTION("static memory pool size with full memory pool") {
        for (int i = 0; true == smpool_aquire(s, (void**)&i); i++);
        REQUIRE(true == smpool_size(s, &size));
        REQUIRE(capacity == size);
    }
    SECTION("static memory pool size with half full memory pool") {
        for (int i = 0, e = 0; (i < (int)(capacity / 2)) && (true == smpool_aquire(s, (void**)&e)); i++);
        REQUIRE(true == smpool_size(s, &size));
        REQUIRE((capacity / 2) == size);
    }
    SECTION("static memory pool size with empty memory pool") {
        REQUIRE(true == smpool_size(s, &size));
        REQUIRE(0 == size);
    }
    SECTION("static memory pool size with memory pool nullptr") {
        REQUIRE(false == smpool_size(NULL, &size));
    }
    SECTION("static memory pool size with element nullptr") {
        REQUIRE(false == smpool_size(s, NULL));
    }
    SECTION("static memory pool clear with memory pool not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static memory pool capacity function tests", "[static_memory pool, smpool_capacity]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(smp, 0, sizeof(smp)));
    REQUIRE(true == smpool_init(smp, capacity, elem_size));
    size_t c = 42;

    SECTION("static memory pool capacity normal") {
        REQUIRE(true == smpool_capacity(smp, &c));
        REQUIRE(capacity == c);
    }
    SECTION("static memory pool capacity with nullptr") {
        REQUIRE(false == smpool_capacity(NULL, &c));
    }
    SECTION("static memory pool capacity with nullptr capacity") {
        REQUIRE(false == smpool_capacity(smp, NULL));
    }
    SECTION("static memory pool capacity with memory pool not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}
