# What is Static Memory Pool ?

Static Memory Pool or short smpool is a mini library, which implements a rudementery memory pool. It uses no heap allocations and the size is fixed at compile time.

## How to use

A basic example can be found below. For more information see the documentation in the header file.

```C
#include "static_memory_pool/static_memory_pool.h"

int main(int argc, char const *argv[])
{
    const size_t capacity = 10;
    const size_t elem_size = sizeof(int)
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    int* elements[capacity];

    if (true == smpool_init(smp, capacity, elem_size))
    {
        // aquire memory from the memory pool
        for (int i = 0; true == smpool_aquire(smp, (void**)elements[i]); i++);

        // perform some actions on the aquired memory
        for (int ii = 0; ii < capacity; ii++)
        {
            int* elem = elements[ii];
            elem[ii] = ii;
        }

        // release the used memory 
        for (int i = 0; smpool_release(smp, (void*)elements[i]); i++);
    }

    return 0;
}
```

## How to Build

1. `$> git clone git@gitlab.com:anomi/static_memory_pool.git`
2. `$> cd static_memory_pool`
3. `$> git submodule update --init --recursive`
4. `$> mkdir build`
5. `$> cd build`
6. `$> cmake ../ && make`
